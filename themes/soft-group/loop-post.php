<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>

        <!-- Blog Post -->
        <!-- Title -->
        <h1><?php the_title(); ?></h1>

        <!-- Author -->
        <p class="lead">
            <?php echo __('by', 'sg'); ?> <a href="#"><?php the_author(); ?></a>
        </p>

        <hr>

        <!-- Date/Time -->
        <p><span class="glyphicon glyphicon-time"></span> <?php echo __('Posted on', 'sg'); ?> <?php the_time('F j, Y @ g:i'); ?></p>

        <hr>

        <!-- Preview Image -->
        <?php if ( has_post_thumbnail() ) { ?>
            <?php the_post_thumbnail(array(900, 300), array('class' => 'img-responsive')); ?>
            <hr>
        <?php } ?>

        <!-- Post Content -->
        <?php the_content(); ?>
        <hr>

        <!-- Blog Comments -->

        <!-- Comments Form -->
        <div class="well">
            <?php comment_form(); ?>
        </div>

        <hr>

    <?php endwhile; ?>
<?php endif; ?>
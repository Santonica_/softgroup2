<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <h2>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        </h2>
        <p class="lead">
            <?php echo __('by', 'sg'); ?> <a href="#"><?php the_author(); ?></a>
        </p>
        <p><span class="glyphicon glyphicon-time"></span> <?php echo __('Posted on', 'sg'); ?> <?php the_time('F j, Y @ H:i:s'); ?></p>
        <hr>
        <?php if ( has_post_thumbnail() ) { ?>
            <?php the_post_thumbnail(array(900, 300), array('class' => 'img-responsive')); ?>
            <hr>
        <?php } ?>
        <p><?php the_excerpt(); ?></p>
        <a class="btn btn-primary" href="<?php the_permalink(); ?>"><?php echo __('Read More' , 'sg'); ?> <span class="glyphicon glyphicon-chevron-right"></span></a>
        <hr>
    <?php endwhile; ?>
<?php endif; ?>
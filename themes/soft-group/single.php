<?php get_header(); ?>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Post Content Column -->
        <div class="col-lg-8">

            <?php get_template_part( 'loop', 'post' ); ?>

        </div>

        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-4">

            <!-- Blog Categories Well -->
            <?php get_sidebar(); ?>

        </div>

    </div>
    <!-- /.row -->

    <hr>

    <?php get_footer(); ?>

</div>
<!-- /.container -->

<?php get_template_part( '/templates/common/html-end' ); ?>

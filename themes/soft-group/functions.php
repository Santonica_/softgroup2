<?php
/**
 * Підключаємо локалізацію шаблону
 */
load_theme_textdomain('sg', get_template_directory() . '/languages');


/**
 * Додаємо favico, charset, viewport
 */
function add_head_meta_tags()
{
    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="<?php bloginfo("template_url"); ?>/favico.png" type="image/x-icon">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
}
add_action('wp_head', 'add_head_meta_tags');


/**
 * Реєструємо місце для меню
 */

register_nav_menus( array(
    'primary' => __('Primary', 'sg'),
) );


/**
 * Реєструємо сайдбари теми
 */
if ( function_exists('register_sidebar') )
{
    register_sidebars(1, array(
        'id' => 'left',
        'name' => __('Left sidebar', 'sg'),
        'description' => '',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<p class="widget-title">',
        'after_title' => '</p>'
    ));

    register_sidebars(1, array(
        'id' => 'right-sidebar',
        'name' => __('Right sidebar', 'sg'),
        'description' => 'right',
        'class' => '',
        'before_widget' => '<div class="well">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));

}


/**
 * Підключаємо підтримку мініатюр
 */
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 150, 150 );
}
/**
 * Можемо добавити різні розміри для картинок
 */
if ( function_exists( 'add_image_size' ) ) { 
    add_image_size( 'photo', 900, 300, true );
}


/**
 * Реєструємо формати постів
 */
function add_post_formats(){
    add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'custom-background' );
    add_theme_support( 'custom-header' );
    add_theme_support( 'custom-logo' );
}
add_action( 'after_setup_theme', 'add_post_formats', 11 );


/**
 * Замінюємо стандартне закінчення обрізаного тексту з [...] на ...
 */
function custom_excerpt_more( $more ) {
	return ' ...';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );


/**
 * Замінюємо стандартну довжину обрізаного тексту
 */
function custom_excerpt_length( $length ) {
	return 200;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


/**
 * Підключаємо javascript файли
 */
function add_theme_scripts(){
    wp_enqueue_script('jquery');
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery') );
    wp_enqueue_script( 'init', get_template_directory_uri() . '/js/init.js', array('jquery', 'bootstrap') );
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );


/**
 * Підключаємо css файли
 */
function add_theme_style(){
    //wp_enqueue_style( 'fonts', get_template_directory_uri() . '/fonts/stylesheet.css');
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style( 'blog-home', get_template_directory_uri() . '/css/blog-home.css');
    wp_enqueue_style( 'blog-post', get_template_directory_uri() . '/css/blog-post.css');
    wp_enqueue_style( 'font', get_template_directory_uri() . '/css/font.css');
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
}
add_action( 'wp_enqueue_scripts', 'add_theme_style' );

class TaxonomyWidget extends WP_Widget
{
    public function __construct() {
        parent::__construct('taxonomy_widget', __('My Taxonomy Widget', 'sg'),
            array('description' => __('A simple widget to get taxonomies', 'sg')));
        add_filter('taxonomies_template', [$this, 'getTaxonomyItemTemplate']);
    }

    public function widget( $args, $instance ) {
        $terms = get_terms([
                'taxonomy' => $instance['taxonomy'],
                'number' => $instance['count'],
        ]);

        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }
        ?>

        <div class="row">
            <div class="col-md-6">
                <ul class="list-unstyled">
                    <?php foreach ($terms as $key => $term) {
                        if( $key % 2 == 0 ) {
                            echo apply_filters('taxonomies_template', $term);
                        }
                    } ?>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="list-unstyled">
                    <?php foreach ($terms as $key => $term) {
                        if( $key % 2 != 0 ) {
                            echo apply_filters('taxonomies_template', $term);
                        }
                    } ?>
                </ul>
            </div>
        </div>

        <?php
        echo $args['after_widget'];
    }

    public function update( $newInstance, $oldInstance ) {
        $values = array();
        $values["title"] = htmlentities( $newInstance["title"] );
        $values["taxonomy"] = htmlentities( $newInstance["taxonomy"] );
        $values["count"] = htmlentities( $newInstance["count"] );
        return $values;
    }

    public function form( $instance ) {
        $title = "";
        $taxonomy = null;
        $count = 10;

        // если instance не пустой, достанем значения
        if ( ! empty( $instance ) ) {
            $title = $instance["title"];
            $taxonomy = $instance["taxonomy"];
            $count = $instance["count"];
        }

        // получаем массив объектов таксономий
        $taxonomies = get_taxonomies(['show_ui' => true, 'show_in_nav_menus' => true, '_builtin' => true], 'object');

        $titleId = $this->get_field_id("title");
        $titleName = $this->get_field_name("title");

        $taxonomyId = $this->get_field_id("taxonomy");
        $taxonomyName = $this->get_field_name("taxonomy");

        $countId = $this->get_field_id("count");
        $countName = $this->get_field_name("count");

        ?>
        <p>
            <label for="<?php echo $titleId; ?>"><?php echo __('Title', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $titleId; ?>" name="<?php echo $titleName; ?>" type="text" value="<?php echo $title; ?>">
        </p>
        <p>
            <label for="<?php echo $taxonomyId; ?>"><?php echo __('Taxonomy', 'sg'); ?></label>
            <select class="widefat" name="<?php echo $taxonomyName; ?>" id="<?php echo $taxonomyId; ?>">
                <?php foreach ($taxonomies as $taxonomyItem){ ?>
                    <option value="<?php echo $taxonomyItem->name; ?>"<?php if($taxonomy == $taxonomyItem->name){ ?> selected="selected"<?php } ?>><?php echo $taxonomyItem->label; ?></option>
                <?php } ?>
            </select>
        </p>
        <p>
            <label for="<?php echo $countId; ?>"><?php echo __('Count', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $countId; ?>" name="<?php echo $countName; ?>" type="number" min="2" value="<?php echo $count; ?>">
        </p>
        <?php
    }

    public function getTaxonomyItemTemplate( $term ) {
        return '<li><a href="' . get_term_link($term) . '">' . $term->name . '</a></li>';
    }

} // class TaxonomyWidget

add_action( "widgets_init", function () {
    register_widget( "TaxonomyWidget" );
});

add_filter ( 'the_content', 'my_parser_filter', 1000 );
function my_parser_filter ($content) {
    require_once 'simple_html_dom.php';
    $html = str_get_html($content);
    $siteUrl = parse_url( get_site_url(), PHP_URL_HOST );
    foreach ( $html->find('a') as $element) {
        $elementHost = parse_url($element->href, PHP_URL_HOST);
        if ( is_null( $elementHost ) || $elementHost == $siteUrl) {
            continue;
        } else {
            $element->target = '_blank';
            $element->href = '/redirect?to=' . $element->href;
        }
    }
    return $html;
}

add_action ('wp_loaded', "my_redirect");
function my_redirect() {
    if( strpos($_SERVER['REQUEST_URI'], '/redirect') === 0 && isset($_GET['to']) ){
        wp_redirect( $_GET['to'] );
        exit;
    }
}

add_filter( 'the_content', 'mySocialShareUrls', 1010 );

add_filter( 'the_excerpt', 'mySocialShareUrls', 1010 );

function mySocialShareUrls( $content ){
    global $post;

    $params = [
        '{url}',
        '{img}',
        '{title}',
        '{desc}',
        '{app_id}',
        '{redirect_url}',
        '{via}',
        '{hashtags}',
        '{provider}',
        '{is_video}'
    ];

    $values = [
        get_permalink(),
        get_the_post_thumbnail_url('full'),
        get_the_title(),
        $post->post_excerpt,
        '',
        get_site_url(),
    ];

    $links = [
        'facebook' => 'https://www.facebook.com/sharer.php?u={url}',
        'twitter' => 'https://twitter.com/intent/tweet?url={url}&text={title}&via={via}&hashtags={hashtags}',
        'google' => 'https://plus.google.com/share?url={url}',
        'linkedin' => 'https://www.linkedin.com/shareArticle?url={url}&title={title}',
        'pinterest' => 'https://pinterest.com/pin/create/bookmarklet/?media={img}&url={url}&is_video={is_video}&description={title}',
    ];

    $linksString = '';
    foreach ( $links as $key => $link ){
        $linksString .= '<a class="' . $key . '" href="' . str_replace($params, $values, $link) . '"></a>';
    }

    return $content . '<div class="social_links">' . $linksString . '</div>';
}

class GetPostsByOptions extends WP_Widget
{
    public function __construct() {
        parent::__construct('PostsByOptions_widget', __('Posts By Options Widget', 'sg'),
            array('description' => __('A simple widget to get posts by options', 'sg')));
    }

    public function widget( $args, $instance ) {

        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }

        $the_query = new WP_Query( array (
                'category__in' => $instance["category"],
                'showposts' => $instance["count"],
        ) );

        remove_filter( 'the_excerpt', 'mySocialShareUrls', 1010 );

        if ( $the_query->have_posts() ){
            echo '<ul class="list-unstyled">';
            while( $the_query->have_posts() ){
                $the_query->the_post();
            ?>
                <li>
                    <p><a  href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                    <?php if ( has_post_thumbnail()) { ?>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                            <?php the_post_thumbnail(); ?>
                        </a>
                    <?php } ?>
                    <p><?php the_excerpt(); ?></p>
                    <div class="row">
                        <div class="col-md-6">
                            <p><?php the_author(); ?></p>
                        </div>
                        <div class="col-md-6">
                            <p><?php the_time('j F Y'); ?></p>
                        </div>
                    </div>
                </li>
                <hr>

            <?php
            }
            echo '</ul>';

            wp_reset_postdata();
        }

        echo $args['after_widget'];
    }

    public function update( $newInstance, $oldInstance ) {
        $values = array();
        $values["title"] = htmlentities( $newInstance["title"] );
        $values["category"] = htmlentities( $newInstance["category"] );
        $values["count"] = htmlentities( $newInstance["count"] );
        return $values;
    }

    public function form( $instance ) {
        $title = "";
        $category = null;
        $count = 5;

        if ( ! empty( $instance ) ) {
            $title = $instance["title"];
            $category = $instance["category"];
            $count = $instance["count"];
        }

        $categories = get_categories();

        $titleId = $this->get_field_id("title");
        $titleName = $this->get_field_name("title");

        $categoryId = $this->get_field_id("category");
        $categoryName = $this->get_field_name("category");

        $countId = $this->get_field_id("count");
        $countName = $this->get_field_name("count");

        ?>
        <p>
            <label for="<?php echo $titleId; ?>"><?php echo __('Title', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $titleId; ?>" name="<?php echo $titleName; ?>" type="text" value="<?php echo $title; ?>">
        </p>
        <p>
            <label for="<?php echo $categoryId; ?>"><?php echo __('Category', 'sg'); ?></label>
            <select class="widefat" name="<?php echo $categoryName; ?>" id="<?php echo $categoryId; ?>">
                <?php foreach ($categories as $cat){ ?>
                    <option value="<?php echo $cat->cat_ID; ?>"<?php selected($category, $cat->cat_ID); ?>><?php echo $cat->name; ?></option>
                <?php } ?>
            </select>
        </p>
        <p>
            <label for="<?php echo $countId; ?>"><?php echo __('Count', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $countId; ?>" name="<?php echo $countName; ?>" type="number" min="2" value="<?php echo $count; ?>">
        </p>
        <?php
    }
} // class GetPostsByOptions

add_action( "widgets_init", function () {
    register_widget( "GetPostsByOptions" );
});

function true_include_myuploadscript() {
    if ( ! did_action( 'wp_enqueue_media' ) ) {
        wp_enqueue_media();
    }
    wp_enqueue_script( 'myuploadscript', get_template_directory_uri() . '/js/upload.js', array('jquery'), null, false );
}

add_action( 'admin_enqueue_scripts', 'true_include_myuploadscript' );

add_action( 'add_meta_boxes', 'add_post_bg_init' );

function add_post_bg_init(){
    add_meta_box( 'post_bg', __('Background','sg'), 'add_post_bg_showup', 'post', 'normal', 'high' );
}

function add_post_bg_showup($post){
    wp_nonce_field("bg_meta_action", "bg_meta_nonce");

    if( function_exists( 'true_image_uploader_field' ) ) {
        true_image_uploader_field( 'uploader_custom', get_post_meta($post->ID, '_uploader_custom',true) );
    }

    ?>
        <p>
            <input type="hidden" name="show_bg" value="0">
            <label><input type="checkbox" name="show_bg" value="1" <?php
            checked( get_post_meta($post->ID, '_show_bg', 1), 1 )?> >Показывать фон?</label>
        </p>

    <?php
}

add_action('save_post', 'add_post_bg_save');

function add_post_bg_save($postID){
    if( !isset($_POST['show_bg']) ){
        return false;
    }
    if (wp_is_post_revision($postID)){
        return false;
    }
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
        return false;
    }
    check_admin_referer("bg_meta_action", "bg_meta_nonce");

    $bg_meta_fields = array(
        '_uploader_custom' => $_POST['uploader_custom'],
        '_show_bg' => $_POST['show_bg'],

    );

    $bg_meta_fields = array_map('trim', $bg_meta_fields);

    foreach( $bg_meta_fields as $key=>$value ){
        update_post_meta( $postID, $key, $value );
    }
}

function true_image_uploader_field( $name, $value = '', $w = 115, $h = 90) {
    $default = get_template_directory_uri() . '/img/no-image.png';
    if( $value ) {
        $image_attributes = wp_get_attachment_image_src( $value, array($w, $h) );
        $src = $image_attributes[0];
    } else {
        $src = $default;
    }
    echo '
	<div>
		<img data-src="' . $default . '" src="' . $src . '" width="' . $w . 'px" height="' . $h . 'px" />
		<div>
			<input type="hidden" name="' . $name . '" id="' . $name . '" value="' . $value . '" />
			<button type="submit" class="upload_image_button button">Загрузить</button>
			<button type="submit" class="remove_image_button button">&times;</button>
		</div>
	</div>
	';
}

add_action('wp_head', 'hook_css_bg');
function hook_css_bg(){
    global $post;
    $postID = $post->ID;
    $show_bg = get_post_meta($postID, '_show_bg', true);
    $uploader = get_post_meta($postID, '_uploader_custom', true);
    if( $show_bg && $uploader ){
        $img = wp_get_attachment_image_url($uploader, 'full');
        echo '<style>.single{ background-image : url(' . $img . '); } </style>';
    }
}

//echo '<pre>';
//var_dump();
//echo '</pre>';
//exit;


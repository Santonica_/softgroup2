<?php get_header(); ?>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="page-header">
                <?php bloginfo('name'); ?>
                <small><?php bloginfo('description'); ?></small>
            </h1>

            <?php get_template_part( 'loop' ); ?>

        </div>

        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-4">

<!--            <!-- Blog Search Well -->
<!--            <div class="well">-->
<!--                <h4>Blog Search</h4>-->
<!--                --><?php //get_search_form(); ?>
<!--            </div>-->

            <!-- Blog Categories Well -->
           <?php get_sidebar(); ?>

        </div>

    </div>
    <!-- /.row -->

    <hr>

    <?php get_footer(); ?>

</div>
<!-- /.container -->

<?php get_template_part( '/templates/common/html-end' ); ?>

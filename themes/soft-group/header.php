<!DOCTYPE>
<html>
  <head>
        <title><?php wp_title(''); ?></title>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
    <header id="header">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only"><?php __('Toggle navigation', 'sg') ?></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <?php wp_nav_menu( array(
                    'theme_location'  => 'primary',
                    'menu'  => 'my test menu',
                    'container'  => 'div',
                    'container_class'  => 'collapse navbar-collapse',
                    'container_id'  => 'bs-example-navbar-collapse-1',
                    'menu_class'  => 'nav navbar-nav',
                ));
                ?>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>
    </header>
<?php
/*
Plugin Name: Twitter-parser
Description: Простой плагин для парсинга Твитера
Version: 1.1
Author: Santonica
*/

function add_plugin_scripts(){
    wp_enqueue_script( 'loadtweet', plugins_url('/js/loadtweet.js', __FILE__), array('jquery') );
    wp_localize_script('loadtweet', 'myajax',
        array(
            'url' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce('tw_ajax_nonce')
        )
    );
}

register_activation_hook(__FILE__, 'plugin_activate');

add_action( 'admin_menu', function(){
    add_menu_page( 'Settings', 'Twitter parser', 8, __FILE__, 'plugin_settings' );
});
add_action( "widgets_init", function () {
    register_widget( "ShowTweets" );
});
add_filter( 'cron_schedules', 'cron_add_five_min' );
add_action( 'my_twitter_hook', 'get_tweets');
add_action( 'init', 'cptui_register_my_cpts_sg_tweets' );
add_action( 'wp_enqueue_scripts', 'add_plugin_scripts' );

/*tw_action_callback - ф-я, tw_action - наш экшен */
add_action('wp_ajax_tw_action', 'tw_action_callback');
add_action('wp_ajax_nopriv_tw_action', 'tw_action_callback');

function tw_action_callback() {
    $nonce = $_POST['nonce'];
    if(!wp_verify_nonce($nonce, 'tw_ajax_nonce')){
        die();
    }
    $result = array();
    $result['last_tw_id'] =  $_POST['last_tw_id'];
    $result['tweets'] =  array();
    $the_query = new WP_Query( array (
        'post_type' => 'sg_tweets',
        'post_status' => 'publish',
        'posts_per_page' => 20,
        'order' => 'ASC',
    ) );
    if ( $the_query->have_posts() ) {
        while ($the_query->have_posts()) {
            $the_query->the_post();
            $post_id = get_the_ID();
            if ($result['last_tw_id'] < $post_id) {
                    $result['last_tw_id'] = $post_id;
                    $tweet_data = get_post_meta($post_id);
                    $result['tweets'][] = array (
                        'tweet_content' => get_the_content($post_id),
                        'profile_image_url' => $tweet_data['profile_image_url'][0],
                        'screen_name' => $tweet_data['screen_name'][0],
                        'user_name' => $tweet_data['user_name'][0],
                        'tweet_date' => $tweet_data['tweet_date'][0],
                    );
            }
        }
        wp_reset_postdata();
    }
    echo json_encode($result);
    wp_die();
}

function plugin_activate(){
   if( !get_option('last_tw')){
       $last_tweet = get_posts(array(
           'numberposts' => 1,
           'post_type' => 'sg_tweets',
           'post_status' => 'publish',
           'posts_per_page' => 20,
       ));

       $last_tweet_id = 0;

       if (!empty($last_tweet)) {
           foreach ($last_tweet as $item) {
               $last_tweet_id = (int)$item->post_title;
           }
       }
       update_option('last_tw', $last_tweet_id);
   }
}

function plugin_settings(){
    $settings_fields = [
        [
            'label' => 'oauth_access_token',
            'name' => 'oauth_access_token',
            'value' => null,
        ],
        [
            'label' => 'oauth_access_token_secret',
            'name' => 'oauth_access_token_secret',
            'value' => null,
        ],
        [
            'label' => 'consumer_key',
            'name' => 'consumer_key',
            'value' => null,
        ],
        [
            'label' => 'consumer_secret',
            'name' => 'consumer_secret',
            'value' => null,
        ],
        [
            'label' => 'screen_name',
            'name' => 'screen_name',
            'value' => null,
        ],
        [
            'label' => 'count',
            'name' => 'count',
            'value' => 10,
        ],
    ];

    if( isset ($_POST['twitter']) ){
        check_admin_referer('update-options');
        $settings =[];
        foreach ($_POST['twitter'] as $field => $value){
            $settings[$field] = $value;
        }
        update_option('tw', $settings);
    }

    $settings = get_option('tw');
    foreach ($settings_fields as &$field){
        $field['value'] = $settings[ $field['name'] ];
    }
    unset($field);
    ?>
    <div class="wrap col-lg-5">
        <h2>Twitter parser</h2>
        <form method="post">
            <?php wp_nonce_field('update-options');

            foreach ($settings_fields as $field){
                ?>
                <div class="wrap">
                    <p>
                        <label><?php echo $field['label']; ?></label>
                        <input class="widefat" name="twitter[<?php echo $field['name']; ?>]" value="<?php echo $field['value']; ?>">
                    </p>
                </div>
                <?php
            }
            ?>
            <input type="hidden" name="action" value="update">
            <input type="hidden" name="page_options" value="tw">

            <p class="submit">
                <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>">
            </p>

        </form>
    </div>
<?php
}

function get_tweets() {
    require_once('TwitterAPIExchange.php');

    /** Set access tokens here - see: https://dev.twitter.com/apps/ **/
    $options = get_option('tw');
    $settings = array(
        'oauth_access_token' => $options['oauth_access_token'],
        'oauth_access_token_secret' => $options['oauth_access_token_secret'],
        'consumer_key' => $options['consumer_key'],
        'consumer_secret' => $options['consumer_secret']
    );

    /** URL for REST request, see: https://dev.twitter.com/docs/api/1.1/ **/
    $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
    $requestMethod = 'GET';
    $getField = '?screen_name=' . $options['screen_name'] . '&count=' . $options['count'];

    $twitter = new \My_twitter\TwitterAPIExchange($settings);
    $response = $twitter->setGetfield($getField)
        ->buildOauth($url, $requestMethod)
        ->performRequest(true, [
            CURLOPT_SSL_VERIFYPEER => false,
        ]);

    $received_tweets = array_reverse(json_decode($response, true));

    $last_tweet_id = get_option('last_tw');

    foreach ($received_tweets as $key => $tweet) {
        if ($last_tweet_id >= $tweet['id']) {
            continue;
        }
        $tweet_data = array(
            'post_title' => $tweet['id'],
            'post_content' => $tweet['text'],
            'post_status' => 'publish',
            'post_type' => 'sg_tweets',
        );

        $tmp_date = $tweet['created_at'];
        $timestamp = strtotime($tmp_date);
        $tw_date = date('F jS, Y @ H:i:s', $timestamp + (get_option( 'gmt_offset' ) * HOUR_IN_SECONDS));

        $post_id = wp_insert_post(wp_slash($tweet_data));
        add_post_meta($post_id, 'user_name', $tweet['user']['name']);
        add_post_meta($post_id, 'screen_name', $tweet['user']['screen_name']);
        add_post_meta($post_id, 'profile_image_url', $tweet['user']['profile_image_url']);
        add_post_meta($post_id, 'tweet_date', $tw_date);
        update_option('last_tw', $tweet['id']);
    }

}

class ShowTweets extends WP_Widget
{
    public function __construct() {
        parent::__construct('ShowTweets_widget', __('Show Tweets Widget', 'sg'),
            array('description' => __('A simple widget to show tweets', 'sg')));
    }

    public function widget( $args, $instance ) {

        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }

        remove_filter( 'the_content', 'mySocialShareUrls', 1010 );

        $the_query = new WP_Query( array (
            'post_type' => 'sg_tweets',
            'post_status' => 'publish',
            'posts_per_page' => $instance["count"],
        ) );

        $last_tw_id = null;

        if ( $the_query->have_posts() ){
            while( $the_query->have_posts() ) {
                $the_query->the_post();
                $post_id = get_the_ID();
                if ($last_tw_id < $post_id) {
                    $last_tw_id = $post_id;
                }
            }

            echo '<ul id="my-tweets" class="list-unstyled" data-lastId="'.$last_tw_id.'">';
            while( $the_query->have_posts() ){
                $the_query->the_post();
                $post_id = get_the_ID();
                $tweet_data = get_post_meta($post_id);
                ?>
                <div id="tweet">
                    <li>
                        <div class="row">
                            <div class="col-md-2">
                                <img src="<?php echo $tweet_data['profile_image_url'][0]; ?>">
                            </div>
                            <div class="col-md-10">
                                <a href="<?php echo 'https://twitter.com/' . $tweet_data['screen_name'][0]; ?>" target="_blank">
                                    <p><?php echo $tweet_data['user_name'][0] . '@' . $tweet_data['screen_name'][0]; ?></p>
                                </a>
                                <p><?php the_content(); ?></p>
                                <p><?php echo $tweet_data['tweet_date'][0]; ?></p>
                            </div>
                        </div>
                    </li>
                    <hr>
                </div>
                <?php
            }
            echo '</ul>';
        }

        wp_reset_postdata();

        echo $args['after_widget'];
    }

    public function update( $newInstance, $oldInstance ) {
        $values = array();
        $values["title"] = htmlentities( $newInstance["title"] );
        $values["count"] = htmlentities( $newInstance["count"] );
        return $values;
    }

    public function form( $instance ) {
        $title = "";
        $count = 5;

        if ( ! empty( $instance ) ) {
            $title = $instance["title"];
            $count = $instance["count"];
        }

        $titleId = $this->get_field_id("title");
        $titleName = $this->get_field_name("title");

        $countId = $this->get_field_id("count");
        $countName = $this->get_field_name("count");

        ?>
        <p>
            <label for="<?php echo $titleId; ?>"><?php echo __('Title', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $titleId; ?>" name="<?php echo $titleName; ?>" type="text" value="<?php echo $title; ?>">
        </p>
        <p>
            <label for="<?php echo $countId; ?>"><?php echo __('Count', 'sg'); ?></label>
            <input class="widefat" id="<?php echo $countId; ?>" name="<?php echo $countName; ?>" type="number" min="2" value="<?php echo $count; ?>">
        </p>
        <?php
    }
} // class ShowTweets

function cron_add_five_min( $schedules ) {
    $schedules['five_min'] = array(
        //'interval' => 60 * 5,
        'interval' => 3,
        'display' => 'Раз в 5 минут'
    );
    return $schedules;
}

//wp_unschedule_event('five_min', 'my_twitter_hook');

if ( ! wp_next_scheduled( 'my_twitter_hook' ) ) {
    wp_schedule_event( time(), 'five_min', 'my_twitter_hook' );
}

function cptui_register_my_cpts_sg_tweets() {

    /**
     * Post Type: tweets.
     */

    $labels = array(
        "name" => __( 'tweets', '' ),
        "singular_name" => __( 'tweet', '' ),
    );

    $args = array(
        "label" => __( 'tweets', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "sg_tweets", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail", "custom-fields" ),
    );

    register_post_type( "sg_tweets", $args );
}
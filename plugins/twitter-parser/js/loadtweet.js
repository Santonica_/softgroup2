jQuery(document).ready(function($) {
    var show = function() {
        var data = {
            action: 'tw_action',
            nonce: myajax.nonce,
            last_tw_id: $('#my-tweets').attr('data-lastId')
        };
        $.ajax({
            url: myajax.url,
            method: 'POST',
            data: data,
            dataType: 'json',
            success: function(res){
                if(res.state == 'fail'){

                } else {
                    $('#my-tweets').attr('data-lastId', res.last_tw_id);
                    if(res.tweets.length > 0) {
                        $.each(res.tweets, function() {
                            var detachedItem = $('#my-tweets #tweet:last-child').detach();
                            detachedItem.find('img').attr('src', this.profile_image_url);
                            detachedItem.find('a').attr('href', 'https://twitter.com/' + this.screen_name);
                            detachedItem.find('.col-md-10 p:not(:empty)').html(this.tweet_content);
                            detachedItem.find('a p').html(this.user_name + '@' + this.screen_name);
                            detachedItem.find('.col-md-10>p:last-child').html(this.tweet_date);
                            detachedItem.prependTo('#my-tweets');
                        });
                    }
                }
            },
            complete: function(res){
                setTimeout(show,10000);
            }
        });
    }
    setTimeout(show,10000);
});